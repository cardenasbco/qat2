# coding: utf-8
"""
Generates and executes high res T2 workflow.

 Nodes: infosource, datagrabber, sink
 Workflows: r2_Check_T2_WF_GM, r2_Check_T2_WF_WM, r2_Check_T2_WF0,
            r2_Check_T2_WF_CSF, r2_Check_T2_WF_Quality
 Inputs: at the moment all inputs are explicitly declared
 Outputs: wm, gm and csf tpms, t2 coregister to t1 and transformation matrices


 arturo@ 2.8.2017
"""


import nipype
import nipype.pipeline.engine as pe
import os
from os.path import join as opj
from nipype.interfaces.utility import Function
import nipype.interfaces.fsl as fsl

# importing my tools
from r2_utils_wip import (r2PcaEigenvectorsFromFirst, r2ScalarProd,
                          r2SformCosDir, r2EVinScannerRF, r2ApparentAngle)
# import nipype.interfaces.utility as util


# defining common variables for all nodes

data_dir = opj(os.environ['HOME'], 'hydra-work/project/testQA_small/')
working_dir = 'Calculations'
base_dir = opj(data_dir, working_dir)
csv_file = opj(data_dir, 'points_physical_space.csv')
"""
# Step 1 - defining first segmentation node

input: inputs.in_file **** < listening, will come from main workflow
output: original_segmentations

method: ('auto' or 'fast' or 'none')
        Method must be one of auto, fast, none, or it can be entered using
        the 'method_as_numerical_threshold' input
        mutually_exclusive: method_as_numerical_threshold

"""

firstNode = nipype.Node(interface=fsl.FIRST(brain_extracted=False),
                        name='firstNode')
firstNode.inputs.out_file = 'FIRST_segmented.nii.gz'
firstNode.inputs.method = 'auto'
firstNode.inputs.environ = {'SGE_ROOT': ''}


"""
# Step 2 - Estimating eigenvectors

input: FIRST output < Step 1 output
"""


r2HcEigenvecNode = pe.Node(name='r2HcEigenvecNode',
                           interface=Function(
                               input_names=['f_name'],
                               output_names=['eigenvec_right',
                                             'eigenvec_left'],
                               function=r2PcaEigenvectorsFromFirst))

"""
Step 3 - Extracting sform and Cosine directors export from header files
"""
r2T1SformCosDirNode = pe.Node(name='r2T1SformCosDirNode',
                              interface=Function(
                               input_names=['f_name'],
                               output_names=['CosDir',
                                             'VecSform',
                                             'CosDirFile',
                                             'VecSformFile'],
                               function=r2SformCosDir))


r2T2SformCosDirNode = pe.Node(name='r2T2SformCosDirNode',
                              interface=Function(
                               input_names=['f_name'],
                               output_names=['CosDir',
                                             'VecSform',
                                             'CosDirFile',
                                             'VecSformFile'],
                               function=r2SformCosDir))

"""
Step 4 - Changing eigenvectors to scanner reference frame
We multiply the eigenvectors obtained from the HC segmentations by the sform
information read from the T1 header to obtain the eigenvectors with respect to
the scanner reference frame.
"""

r2EVinScannerRFNode = pe.Node(name='r2EVinScannerRFNode',
                              interface=Function(
                               input_names=['T1VecSform',
                                            'EVl',
                                            'EVr'],
                               output_names=['EVl_srf',
                                             'EVr_srf'],
                               function=r2EVinScannerRF))

"""
Step 5 - Scalar product, estimating angles
Estimating scalar product between the principal axis of each hippocampus
and the T2slice normal vector.
"""
r2ScalarProdNode = pe.Node(name='r2ScalarProdNode',
                           interface=Function(
                               input_names=['eigenvec_right',
                                            'eigenvec_left',
                                            'T2CosDir', 'subject_id'],
                               output_names=['alpha_left_deg',
                                             'alpha_right_deg'],
                               function=r2ScalarProd))
r2ApparentAngleNode = pe.Node(name='r2ApparentAngleNode',
                              interface=Function(
                               input_names=['eigenvec_right',
                                            'eigenvec_left',
                                            'T2CosDir', 'T1CosDir',
                                            'subject_id'],
                               output_names=['apparent_alpha_left_deg',
                                             'apparent_alpha_right_deg'],
                               function=r2ApparentAngle))
"""
defining workflow
"""
# --- previous workflow version ---
r2First_WF_wip = nipype.Workflow(name='r2First_WF')
r2First_WF_wip.connect([(firstNode, r2HcEigenvecNode,
                         [('segmentation_file', 'f_name')]),
                        (r2HcEigenvecNode, r2EVinScannerRFNode,
                         [('eigenvec_right', 'EVr'),
                          ('eigenvec_left', 'EVl')]),
                        (r2T1SformCosDirNode, r2EVinScannerRFNode,
                         [('VecSform', 'T1VecSform')]),
                        (r2EVinScannerRFNode, r2ScalarProdNode,
                         [('EVl_srf', 'eigenvec_left'),
                          ('EVr_srf', 'eigenvec_right')]),
                        (r2T2SformCosDirNode, r2ScalarProdNode,
                         [('CosDir', 'T2CosDir')]),
                        (r2EVinScannerRFNode, r2ApparentAngleNode,
                         [('EVl_srf', 'eigenvec_left'),
                          ('EVr_srf', 'eigenvec_right')]),
                        (r2T2SformCosDirNode, r2ApparentAngleNode,
                         [('CosDir', 'T2CosDir')]),
                        (r2T1SformCosDirNode, r2ApparentAngleNode,
                         [('CosDir', 'T1CosDir')])])
