
### Description

Processing pipeline developed to automatically assess the quality of specific structural T2-weighted images
typically acquired in the study of the hippocampus. By combining existing neuroimaging tools, the presented pipeline generates descriptive information about the signal properties in different tissue classes of the T2-weighted image. This information could subsequently be used to detect sub-optimal volumes due to noise or motion artifacts. Similarly as it measures the angulation of the T2-weighted slices with respect to the HC, it could also be used to automatically determine whether the field of view angulation follows protocol.


![alt text](graph_old.png "Complete Pipeline")
