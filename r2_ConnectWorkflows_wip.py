# coding: utf-8
"""
Generates and executes high res T2 workflow.

 Nodes: infosource, datagrabber, sink
 Workflows - filename: SkullStripCoreg - r2_Check_T2_WF0,
 workflow_quality - r2_Check_T2_WF_Quality
 r2First_WF - r2_First
 Inputs: at the moment all inputs are explicitly declared
 Outputs: wm, gm and csf tpms, t2 coregister to t1 and transformation matrices

 aim of this second phase: After generating some preliminary results for ISMRM,
 my intention now is to finalise a formal beta version of this automatic QA
 tool.
Desired elements:
1 - At the moment, without correction, the code generates the wrong set of
       angles. we need to incorporate the changes to properly measure
       orientation angles.
2 - After processing the initial datasets, I noticed that therewere some
       subjects for which there were no signal values. why? what is the issue
       with those?
3 - Generate report, include some images, ilustrate segmentation accuracy and
       coregistration
4 - wrap it up in a docker image

While we adress these issues - save figures for publication. w
 arturo@ 15.1.2018
"""

import nipype
import nipype.pipeline.engine as pe
import nipype.interfaces.io as nio
import os
from os.path import join as opj
import nipype.interfaces.utility as util
# importing workflows
from r2_Check_T2_WF_Quality import workflow_quality
from r2_Check_T2_WF0 import SkullStripCoreg
from r2_FIRST_wip import r2First_WF_wip
# to be done: add r2qi1 to WF
from r2_qi1 import r2_qi1_WF

# Cloning workflows
workflow_quality_wm = workflow_quality.clone('workflow_quality_wm')
workflow_quality_csf = workflow_quality.clone('workflow_quality_csf')
# Defining the tpm variable inside each of the cloned workflows
# workflow_quality_gm.inputs.tpm = 1
# workflow_quality_gm.inputs.tpm = 1
# workflow_quality_wm.inputs.tpm = 2
# workflow_quality_wm.inputs.tpm = 2
# workflow_quality_csf.inputs.tpm = 3
# workflow_quality_csf.inputs.tpm = 3
# defining common variables for all nodes
# laptop configuration
# base_dir = os.path.join(os.environ['HOME'], 'project')
# output_dir = '/Users/acardenas/project/'
# hydra configuration

# used for testing sFile = '/home/acardenas/hydra-work/project/testQA_good/Verified_subjects.txt'
# used for testing iTemplate = '/home/acardenas/hydra-work/project/testQA_good/%s/t%s'
# used for testing data_dir = os.path.join(os.environ['HOME'], 'hydra-work/project/testQA_good/')
sFile = '/home/acardenas/hydra-work/180523-ASHS-MoCo/runningQA/Verified_subjects.txt'
iTemplate = '/home/acardenas/hydra-work/180523-ASHS-MoCo/runningQA/%s/t%s'
data_dir = os.path.join(os.environ['HOME'], 'hydra-work/180523-ASHS-MoCo/runningQA/')
working_dir = 'Calculations_new'
base_dir = opj(data_dir, working_dir)

# defining sink node
sink = nipype.Node(interface=nipype.DataSink(),
                   name='sink')
sink.inputs.base_directory = os.path.join(base_dir, 'output')
# substitutions = [('_subject_id_', '')]

substitutions = [('_subject_id_', '')]
sink.inputs.substitutions = substitutions

"""
Defining datagrabber - This time in order to run subject ids as iterables we
introduce a new node called "infosource" which is the one that passes the
subject_id fields to the datagrabber.
"""
# Specify the subject directories
# subject_list = ['subj1', 'subj2', 'subj3']
# adding list of subjects from a text file:
SubjectsList = []
f = open(sFile)
for line in f:
    field = line.strip().split()
    SubjectsList.append(field[0])
f.close()
# Used below as part of the infosource.iterables
subject_list = SubjectsList  # ['0a71a953d', ..., '0df733308']

"""
Here we set up iteration over all the subjects. The following line is a
particular example of the flexibility of the system. The datasource
attribute iterables tells the pipeline engine that it should repeat
the analysis on each of the items in the subject_list. In the current
example, the entire first level preprocessing and estimation will be
repeated for each subject contained in subject_list. Now we create a
nipype.interfaces.io.DataGrabber object and fill in the information from
above about the layout of our data. The nipype.pipeline.NodeWrapper module
wraps the interface object and provides additional housekeeping and pipeline
specific functionality.
"""

infosource = pe.Node(util.IdentityInterface(fields=['subject_id']),
                     name="infosource")
infosource.iterables = [('subject_id', subject_list)]
grabber = pe.Node(nio.DataGrabber(infields=['subject_id'],
                                  outfields=['t1', 't2',
                                             'header_t1', 'header_t2']),
                  base_directory=base_dir,
                  sort_files=True,
                  name='grabber')
grabber.inputs.template = iTemplate
info = dict(t1=[['subject_id', '1.nii']], t2=[['subject_id', '2.nii']],
            header_t1=[['subject_id', '1_header.txt']],
            header_t2=[['subject_id', '2_header.txt']])
grabber.inputs.template_args = info
grabber.inputs.sort_filelist = True


HighresT2QA = nipype.Workflow(name='HighresT2QA')
# HighresT2QA.base_dir = base_dir
HighresT2QA.connect([(infosource, grabber,
                      [('subject_id', 'subject_id')]),
                     (grabber, SkullStripCoreg,
                      [('t1', 'NewSeg.channel_files'),
                       ('t2', 'coreg.moving_image'),
                       ('t2', 'warpmean.reference_image'),
                       ('t1', 'warpmean.input_image')]),
                     (grabber, workflow_quality,
                      [('t2', 'warpmean.reference_image'),
                       ('t2', 'applymask.in_file')]),
                     (SkullStripCoreg, workflow_quality,
                      [('coreg.composite_transform', 'warpmean.transforms'),
                       ('r2getclassNimageNode.comboNimage1tpm',
                        'r2undoComboNode.ComboImagetpm')]),
                     (infosource, workflow_quality,
                      [('subject_id', 'r2FastTPMStatsNode.subject_id')]),
                     (grabber, workflow_quality_wm,
                      [('t2', 'warpmean.reference_image'),
                       ('t2', 'applymask.in_file')]),
                     (SkullStripCoreg, workflow_quality_wm,
                      [('coreg.composite_transform', 'warpmean.transforms'),
                       ('r2getclassNimageNode.comboNimage2tpm',
                        'r2undoComboNode.ComboImagetpm')]),
                     (infosource, workflow_quality_wm,
                      [('subject_id', 'r2FastTPMStatsNode.subject_id')]),
                     (grabber, workflow_quality_csf,
                      [('t2', 'warpmean.reference_image'),
                       ('t2', 'applymask.in_file')]),
                     (SkullStripCoreg, workflow_quality_csf,
                      [('coreg.composite_transform', 'warpmean.transforms'),
                       ('r2getclassNimageNode.comboNimage3tpm',
                        'r2undoComboNode.ComboImagetpm')]),
                     (SkullStripCoreg, r2_qi1_WF,
                      [('r2getclassNimageNode.comboNimage6tpm',
                        'r2HistandThreNode.f_name')]),
                     (infosource, r2_qi1_WF,
                      [('subject_id', 'r2qi1Node.subject_id')]),
                     (infosource, workflow_quality_csf,
                     [('subject_id', 'r2FastTPMStatsNode.subject_id')]),
                     (grabber, r2First_WF_wip,
                      [('t1', 'firstNode.in_file'),
                       ('header_t1', 'r2T1SformCosDirNode.f_name'),
                       ('header_t2', 'r2T2SformCosDirNode.f_name')]),
                     (infosource, r2First_WF_wip,
                      [('subject_id', 'r2ScalarProdNode.subject_id')]),
                     (infosource, r2First_WF_wip,
                      [('subject_id', 'r2ApparentAngleNode.subject_id')]),
                     (SkullStripCoreg, sink,
                      [('NewSeg.native_class_images', '1-NewSegment'),
                       ('coreg.warped_image', '3-T1T2coreg'),
                       ('coreg.inverse_warped_image',
                        '3-T1T2coreg-inverse')]),
                     (workflow_quality, sink,
                      [('applymask.out_file', '5-BGSegmentonT2'),
                       ('r2FastTPMStatsNode.fig_name',
                        '60-SignalDistributions'),
                       ('erodeBin.out_file',
                        '62-ErodedTpm'),
                       ('r2FastTPMStatsNode.output_fname',
                        '61-SignalVectorFiles')]),
                     (workflow_quality_wm, sink,
                      [('applymask.out_file', '5-BGSegmentonT2.@bla'),
                       ('r2FastTPMStatsNode.fig_name',
                        '60-SignalDistributions.@bla'),
                       ('erodeBin.out_file',
                        '62-ErodedTpm.@bla'),
                       ('r2FastTPMStatsNode.output_fname',
                        '61-SignalVectorFiles.@bla')]),
                     (workflow_quality_csf, sink,
                      [('applymask.out_file', '5-BGSegmentonT2.@bla2'),
                       ('r2FastTPMStatsNode.fig_name',
                        '60-SignalDistributions.@bla2'),
                       ('erodeBin.out_file',
                        '62-ErodedTpm.@bla2'),
                       ('r2FastTPMStatsNode.output_fname',
                        '61-SignalVectorFiles.@bla2')])])
#                     (r2First_WF, sink,
#                      [('antsApplyTransPoints.output_file', '77-pts'),
#                       ('r2ScalarProdNexport ode.alpha_left', '777-angle'),
#                       ('r2ScalarProdNode.alpha_right', '777-angle-R')])])
#                        ('erodeBin.out_file',
#                       '62-ErodedTpm'),

# generating grapho
HighresT2QA.write_graph(graph2use='flat', format='png')
HighresT2QA.run('MultiProc', plugin_args={'n_procs': 10})
# running script :-)
# scaffoldflow.run('Linear')
# scaffoldflow.run('MultiProc', plugin_args={'n_procs': 20})
# scaffoldflow.run(plugin='SGE', plugin_args={qsub_args='-S /bin/bash -v
