
# coding: utf-8
"""
Generates skullstrip and coregister, t2 and T1 images.

 Nodes: bet, fast, coreg, warpmean, select
 Inputs: 2 in total. t1 and t2 images
 Outputs: wm, gm and csf tpms, t2 co-reg'ed to t1 and transformation matrices

 arturo@ 31.7.2017
"""

import nipype
import nipype.interfaces.ants as ants
import nipype.interfaces.spm as spm
from nipype.interfaces.utility import Function
from r2_utils_wip import r2getclassNimage

# import nipype.interfaces.spm as spm


# Step 0 - new segment as an alternative to bet and fast

NewSeg = nipype.Node(interface=spm.NewSegment(), name='NewSeg')
NewSeg.inputs.channel_info = (0.001, 60, (True, True))
tissue1 = (('/opt/spm/spm12/tpm/TPM.nii', 1), 2, (True, False), (False, False))
tissue2 = (('/opt/spm/spm12/tpm/TPM.nii', 2), 2, (True, False), (False, False))
tissue3 = (('/opt/spm/spm12/tpm/TPM.nii', 3), 2, (True, False), (False, False))
tissue4 = (('/opt/spm/spm12/tpm/TPM.nii', 4), 2, (True, False), (False, False))
tissue5 = (('/opt/spm/spm12/tpm/TPM.nii', 5), 2, (True, False), (False, False))
tissue6 = (('/opt/spm/spm12/tpm/TPM.nii', 6), 2, (True, False), (False, False))
NewSeg.inputs.tissues = [tissue1, tissue2, tissue3, tissue4, tissue5, tissue6]

# Step 3 - Coregistration

# Coregistration t1 and t2 volumes ** to be added
coreg = nipype.Node(interface=ants.Registration(), name='coreg')
coreg.inputs.output_transform_prefix = 't2ont1'
coreg.inputs.output_warped_image = 't2ont1Warped.nii.gz'
coreg.inputs.output_inverse_warped_image = 't1ont2Warped.nii.gz'
coreg.inputs.output_transform_prefix = "t2ont1_"
coreg.inputs.transforms = ['Rigid', 'Rigid']
coreg.inputs.transform_parameters = [(0.1,), (0.1,)]
coreg.inputs.number_of_iterations = [[100, 100]]*3
coreg.inputs.dimension = 3
coreg.inputs.write_composite_transform = True
coreg.inputs.collapse_output_transforms = False
coreg.inputs.metric = ['MI'] * 2
coreg.inputs.metric_weight = [1] * 2
coreg.inputs.radius_or_number_of_bins = [32] * 2
coreg.inputs.sampling_strategy = ['Regular'] * 2
coreg.inputs.sampling_percentage = [0.3] * 2
coreg.inputs.convergence_threshold = [1.e-8] * 2
coreg.inputs.convergence_window_size = [20] * 2
coreg.inputs.smoothing_sigmas = [[4, 2]] * 2
coreg.inputs.sigma_units = ['vox'] * 4
coreg.inputs.shrink_factors = [[6, 4]] + [[3, 2]]
coreg.inputs.use_estimate_learning_rate_once = [True] * 2
coreg.inputs.use_histogram_matching = [False] * 2
coreg.inputs.initial_moving_transform_com = True


# #### Applying the warp####
# Step 4 - Apply Warp:
"""
Careful here, it will do the same
for all the tissue segments extracted from bet
"""
warpmean = nipype.Node(ants.ApplyTransforms(),
                       name='warpmean')
warpmean.inputs.input_image_type = 0
warpmean.inputs.interpolation = 'Linear'
warpmean.inputs.invert_transform_flags = [True]
# warpmean.inputs.terminal_output = 'file'
warpmean.inputs.output_image = 'T1onT2.nii.gz'

# Selecting spm tpm - Background
r2getclassNimageNode = nipype.Node(name='r2getclassNimageNode',
                                   interface=Function(input_names=[
                                            'spm_class_image'],
                                            output_names=['comboNimage1tpm',
                                                          'comboNimage2tpm',
                                                          'comboNimage3tpm',
                                                          'comboNimage6tpm'],
                                            function=r2getclassNimage))


# Connecting the nodes
SkullStripCoreg = nipype.Workflow(name='SkullStripCoreg')
SkullStripCoreg.connect([(NewSeg, coreg, [('bias_corrected_images',
                                           'fixed_image')]),
                         (coreg, warpmean,
                         [('composite_transform', 'transforms')]),
                         (NewSeg, r2getclassNimageNode,
                         [('native_class_images', 'spm_class_image')])])
SkullStripCoreg.write_graph(graph2use='colored', format='png')
