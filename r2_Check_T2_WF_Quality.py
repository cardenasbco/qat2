# coding: utf-8
"""
Generates background mask, hisotgram and saves bakground data vector.

 Nodes: r2Files2listNode, joinMasks, getBack, r2MaskStatsNode,
 Inputs: 4 in total. quantitative image file, CSF, WM and GM mask files
 Outputs: background mask, histogram and data vector

 arturo@ 31.7.2017
"""
import nipype.pipeline.engine as pe
import os
from os.path import join as opj
import nipype
import nipype.interfaces.fsl as fsl
from nipype.interfaces.utility import Function
from r2_utils_wip import (r2FastTPMStats, r2ShapiroandStats)
from r2_utils_wip import (r2undoCombo)
from nipype.interfaces import ants
# import nipype.interfaces.utility as util
# from nipype.interfaces.fsl import MultiImageMaths
# import nipype.interfaces.utility as util
data_dir = opj(os.environ['HOME'], 'hydra-work/project/testQA_small/')
working_dir = 'Calculations'
base_dir = opj(data_dir, working_dir)
# Selecting spm tpm - Background
# Once the function has been defined we use it as a Node.
r2undoComboNode = pe.Node(name='r2undoComboNode',
                          interface=Function(input_names=[
                                'ComboImagetpm'],
                                 output_names=['ima_file',
                                               'tpm'],
                                 function=r2undoCombo))


# deleted - here was the get class image function
# eroding and binarising spm tpm
erodeBin = pe.Node(interface=fsl.maths.MathsCommand(), name='erodeBin')
erodeBin.inputs.args = ' -kernel gauss 0.9 -ero -thr 0.9999 -bin'
erodeBin.terminal_output = 'file'
# eroFname = 'TPM_' + str(tpm) + 'eroded.nii.gz'
erodeBin.out_file = 'eroded.nii.gz'

# Warping to T2 space
warpmean = nipype.Node(ants.ApplyTransforms(),
                       name='warpmean')

warpmean.inputs.input_image_type = 0
warpmean.inputs.interpolation = 'NearestNeighbor'
warpmean.inputs.invert_transform_flags = [True]
# warpmean.inputs.output_image = 'BinBGonT2.nii.gz'


# Brain segmentation needs to be applied to a skullstripped image
applymask = nipype.Node(interface=fsl.maths.ApplyMask(), name='applymask')
# applymask.inputs.out_file = 'BGonT2.nii.gz'


# Once the function has been defined we use it as a Node.
r2FastTPMStatsNode = pe.Node(name='r2FastTPMStatsNode',
                             interface=Function(input_names=[
                                 'f_fname', 'subject_id',
                                 'tpm'],
                                 output_names=['data_vector',
                                               'subject_id',
                                               'fig_name',
                                               'output_fname'],
                                 function=r2FastTPMStats))

# r2FastTPMStatsNode.inputs.tpm = tpm
# r2FastTPMStatsNode.inputs.output_dir = base_dir # this was active


# calling r2ShapiroandStats
r2ShapiroandStatsNode = pe.Node(name='r2ShapiroandStatsNode',
                                interface=Function(input_names=[
                                    'data_vector', 'subject_id',
                                    'tpm'],
                                    output_names=[],
                                    function=r2ShapiroandStats))
# r2ShapiroandStatsNode.inputs.tpm = tpm

# Defining workflow
workflow_quality = nipype.Workflow(name='workflow_quality')
workflow_quality.connect([(r2undoComboNode, erodeBin,
                           [('ima_file', 'in_file')]),
                          (r2undoComboNode, r2FastTPMStatsNode,
                           [('tpm', 'tpm')]),
                          (r2undoComboNode, r2ShapiroandStatsNode,
                           [('tpm', 'tpm')]),
                          (erodeBin, warpmean,
                           [('out_file', 'input_image')]),
                          (warpmean, applymask,
                           [('output_image', 'mask_file')]),
                          (applymask,
                           r2FastTPMStatsNode, [('out_file', 'f_fname')]),
                          (r2FastTPMStatsNode,
                           r2ShapiroandStatsNode, [('data_vector',
                                                    'data_vector')]),
                          (r2FastTPMStatsNode,
                           r2ShapiroandStatsNode, [('subject_id',
                                                    'subject_id')])])
