#!/home/acardenas/miniconda36/bin/python

import sys
import getopt
import os
import subprocess


def walklevel(some_dir, level=1):
    """Adapt os.walk to run only into one level."""
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]


def main(argv):
    """Check dependencies and generates list of verified subjects.
    Flags:
    -i: input folder
    -o: output folder

    This script checks the subfolders inside $PWD. If both the T1
    and the T2 weighted images are in place, the subject name will be appended
    to a text file into the output folder - `Verified_subjects.txt`. On the
    contrary, the subject name will be appended to - `Problematic_subjects.txt`.
    """
    inputDir = ''
    outputDir = ''
    print("hola")
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["iDir=", "oDir="])
    except getopt.GetoptError:
        print("CheckingDependencies.py -i <inputDir> -o <outputDir>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("CheckingDependencies.py -i <inputDir> -o <outputDir>")
            sys.exit()
        elif opt in ("-i", "--iDir"):
            inputDir = arg
        elif opt in ("-o", "--oDir"):
            outputDir = arg
    print(" +++++++")
    print(" - Input directory is: {}".format(inputDir))
    print(" - Output directory is: {}".format(outputDir))
    print(" +++++++")

    for dirName, subdirList, fileList in walklevel(inputDir):
        dirNameStr = os.path.basename(dirName)
        print("%s - Directory found" % dirName)
        if 't1.nii' and 't2.nii' in fileList:
            print('Both files found in %s' % dirNameStr)
            goodFile = outputDir + "/Verified_subjects.txt"
            with open(goodFile, "a") as text_file:
                text_file.write(dirNameStr)
                text_file.write("\n")
            localPath = dirName
            ActualCall_t1 = ["fslhd " + localPath + "/t1.nii >>" +
                             localPath + "/t1_header.txt"]
            ActualCall_t2 = ["fslhd " + localPath + "/t2.nii >>" +
                             localPath + "/t2_header.txt"]
            # print('bla %s' % ActualCall_t1)
            subprocess.call(ActualCall_t1, shell=True)
            subprocess.call(ActualCall_t2, shell=True)
        else:
            print('Problem found in %s' % dirNameStr)
            badFile = outputDir + "/Problematic_subjects.txt"
            with open(badFile, "a") as text_file:
                text_file.write(dirNameStr)
                text_file.write("\n")


if __name__ == "__main__":
    main(sys.argv[1:])
