"""
Used to count the number of artefactual voxels in the
image background.

arturo@24.04.2018
testing changes 
dummy change
"""

import nipype
import nipype.pipeline.engine as pe
import nipype.interfaces.fsl as fsl
from nipype.interfaces.utility import Function
from r2_utils_wip import r2HistogramandThreshold, r2qi1Count


"""
# Step 1 - Estimating most frequent value

input: Background signal intensity comes from main Workflow
output: HistThre
"""


r2HistandThreNode = pe.Node(name='r2HistandThreNode',
                            interface=Function(
                               input_names=['f_name'],
                               output_names=['HistThre', 'f_name'],
                               function=r2HistogramandThreshold))

"""
# Step 2 - thresholding image

input: inputs.in_file **** < listening, will come from main workflow
ThrNode.inputs.thresh
output: out_file

"""

ThrNode = nipype.Node(interface=fsl.maths.Threshold(),
                      name='ThrNode')


ThrNode2 = nipype.Node(interface=fsl.maths.Threshold(),
                       name='ThrNode2')
"""
# Step 3 - eroding image

input: inputs.in_file **** < listening, will come from main workflow
ThrNode.inputs.thresh
output: out_file

"""
EroNode = nipype.Node(interface=fsl.maths.ErodeImage(),
                      name='EroNode')
EroNode.inputs.kernel_shape = 'sphere'
EroNode.inputs.kernel_size = 2

"""
# Step 4 - Dilate image
input: inputs.in_file **** < listening, will come from main workflow
ThrNode.inputs.thresh
output: out_file

"""
DilNode = nipype.Node(interface=fsl.maths.DilateImage(),
                      name='DilNode')
DilNode.inputs.kernel_shape = 'sphere'
DilNode.inputs.kernel_size = 2
DilNode.inputs.operation = 'mean'
"""
# Step 5 - Estimating qi1

input: Background signal intensity comes from main Workflow
output: HistThre
"""


r2qi1Node = pe.Node(name='r2qi1Node',
                    interface=Function(input_names=['background_ima',
                                                    'processed_ima',
                                                    'subject_id'],
                                       output_names=['qi1'],
                                       function=r2qi1Count))


# Workflow
r2_qi1_WF = nipype.Workflow(name='r2_qi1')
r2_qi1_WF.connect([(r2HistandThreNode, ThrNode,
                    [('HistThre', 'thresh'),
                     ('f_name', 'in_file')]),
                   (ThrNode, EroNode,
                    [('out_file', 'in_file')]),
                   (EroNode, ThrNode2,
                   [('out_file', 'in_file')]),
                   (r2HistandThreNode, ThrNode2,
                    [('HistThre', 'thresh')]),
                   (ThrNode2, DilNode,
                    [('out_file', 'in_file')]),
                   (DilNode, r2qi1Node,
                    [('out_file', 'processed_ima')]),
                   (r2HistandThreNode, r2qi1Node,
                    [('f_name', 'background_ima')])])
#r2_qi1_WF.write_graph(graph2use='colored', format='png')
