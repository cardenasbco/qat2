# coding: utf-8
"""
Stores a set of utility funcitons developed by acb.

 r2FastTPMStats (f_fname, subject_id, tpm)
 arturo@ 2.8.2017
"""


def r2EVinScannerRF(T1VecSform, EVl, EVr):
    """
    Read eigenvector from scanner's reference frame.

    Takes Sform information from T1-weighted image and Hippocampal PCA's
    eigenvectors and converts the Eigenvectors to the scanner reference frame.
    """
    Sform = T1VecSform
    import numpy as np
    EVl = np.append(EVl, [1])
    EVr = np.append(EVr, [1])
    # transposing vectos
    EVlT = np.transpose(EVl)
    EVrT = np.transpose(EVr)
    # multiplying matrix times vector and transposing results
    Newl = np.matmul(Sform, EVlT)
    Newr = np.matmul(Sform, EVrT)
    # transposing
    EVl_srf = np.transpose(Newl[0:3])
    EVr_srf = np.transpose(Newr[0:3])
    return EVl_srf, EVr_srf


def r2SformCosDir(f_name):
    """Read the sform information from the nifti header.

    Uses the sform information and the pixel dimensions to estimate the
    direction cosines.
    The sform information, as especified in the dicom-nifti documentation can
    be used to move from the image reference frame into the scanner reference
    frame.
                        [Px]  [F11*Dr F12*Dc 0 Sx] [r]
                        [Py]= [F21*Dr F22*Dc 0 Sy] [c]
                        [Pz]  [F31*Dr F32*Dc 0 Sz] [0]
                        [1 ]  [0      0      0  1] [1]

    P_{xyz} = Coordinates of the voxel (c,r) in the frame's image plane in
    units of mm S_{xyz} = The three values of the image position (patient)
    attributes. It is the location in mm from the origin of the RCS F_{:1}  =
    the value from the column (Y) direction cosine of the Image Orientation
    (Patient) (0020,0037) see above.
    F_{:,2} =  the values of the rows (X) direction cosine
    r       = row index to the image plane
    Dr      = Row pixel resolution of the pixel spacing (0028,0030) in mm
    c       = column index to the image plane. First column index is zero
    Dc      = column pixel resolution of the pixel spacing (0028,0030) in mm

    more info:
    http://nipy.org/nibabel/dicom/dicom_orientation.html

    """
    import re
    import numpy as np
    import os
    fname = f_name
    try:
        f = open(fname)
        for line in f:
            if re.match(('sto_xyz:1'), line):
                Vec1 = re.findall(r'[-+]?\d*\.\d+|\d+', line)
                Vec1 = list(map(float, Vec1))
                Vec1 = Vec1[1:]
            elif re.match(('sto_xyz:2'), line):
                Vec2 = re.findall(r'[-+]?\d*\.\d+|\d+', line)
                Vec2 = list(map(float, Vec2))
                Vec2 = Vec2[1:]
            elif re.match(('sto_xyz:3'), line):
                Vec3 = re.findall(r'[-+]?\d*\.\d+|\d+', line)
                Vec3 = list(map(float, Vec3))
                Vec3 = Vec3[1:]
            elif re.match(('sto_xyz:4'), line):
                Vec4 = re.findall(r'[-+]?\d*\.\d+|\d+', line)
                Vec4 = list(map(float, Vec4))
                Vec4 = Vec4[1:]
            elif re.match(('pixdim1'), line):
                Pdim1 = re.findall(r'[-+]?\d*\.\d+|\d+', line)
                Pdim1 = list(map(float, Pdim1))
                Pdim1 = Pdim1[1:]
            elif re.match(('pixdim2'), line):
                Pdim2 = re.findall(r'[-+]?\d*\.\d+|\d+', line)
                Pdim2 = list(map(float, Pdim2))
                Pdim2 = Pdim2[1:]
        f.close()
    except:
        Vec1 = Vec2 = Vec3 = Vec4 = [0, 0, 0, 0]
        Pdim1 = Pdim2 = [1]
        # Stacking them up together and converting to string
    VecSform = np.vstack((Vec1, Vec2, Vec3, Vec4))
    VecSformFile = 'OrientationInformation.txt'
    np.savetxt(VecSformFile, VecSform, delimiter=',')
    # extracting director cosines
    temp1 = VecSform[0:3, 0]
    # applying sign convention
    SignConvention1 = np.array([-1, 1, 1])
    temp1 = np.multiply(temp1, SignConvention1)
    # Correcting for pixel dimension
    temp1 = np.array(temp1)/Pdim1
    # Repeating the same process for the second Cosine director
    temp2 = VecSform[0:3, 1]
    SignConvention2 = np.array([1, -1, 1])
    temp2 = np.multiply(temp2, SignConvention2)
    temp2 = np.array(temp2)/Pdim2
    # stacking them together and converting to string
    CosDir = np.vstack((temp1[0:3], temp2[0:3]))
    # CosDirStr = np.array2string(CosDir)
    CosDirFile = 'CosineDirectors.txt'
    np.savetxt(CosDirFile, CosDir, delimiter=',')
    # saving the file
    np.savetxt(CosDirFile, CosDir, delimiter=',')
    return (CosDir, VecSform, os.path.abspath(CosDirFile),
            os.path.abspath(VecSformFile))


def r2FastTPMStats(f_fname, subject_id, tpm):
    """Estimate TPM stats."""
    from nibabel import load
    import numpy as np
    import matplotlib
    import os
    matplotlib.use('agg')
    import matplotlib.pyplot as plt
    """Given data, generates histograms and vector files."""
    # importing necessary libraries
    # loading image into array
    img = load(f_fname).get_data()
    # making 0 values equal to NaN
    img[img == 0] = np.nan
    img.shape
    # reshaping image from 3D matrix to 1D vectorbare
    iimag = np.reshape(img, img.shape[0] * img.shape[1] * img.shape[2])
    fig = plt.figure()
    ax = fig.add_subplot(111)
    # Generating histogram only for those voxels with values different from NaN
    ax.hist(iimag[~np.isnan(iimag)], bins=100, normed=True)
    # ax.xlabel("Value")
    # ax.ylabel("Frequency")
    # output_dir = '/home/acardenas/hydra-work/project/'
    if tpm == 3:  # 0 bet
        # CSF
        output_fname = 'csf.csv'
        fig_name = 'csf.png'
    elif tpm == 1:
        output_fname = 'gm.csv'
        fig_name = 'gm.png'
    elif tpm == 2:
        output_fname = 'wm.csv'
        fig_name = 'wm.png'
    elif tpm == 6:  # 3 bet
        output_fname = 'backG.csv'
        fig_name = 'backG.png'
    data_vector = iimag[~np.isnan(iimag)]
    fig.savefig(fig_name)
    np.savetxt(output_fname, data_vector, delimiter=",")
    # fig_nombre = os.path.abspath(fig_name)
    # output_nombre = os.path.abspath(output_fname)
    # return (data_vector, subject_id)bare
    return (data_vector, subject_id, os.path.abspath(fig_name),
            os.path.abspath(output_fname))


def r2PcaEigenvectorsFromFirst(f_name):
    """Extract the main eigenvector after running PCA for a masked file."""
    import nibabel as nib
    import numpy as np
    from sklearn.decomposition import PCA
    img = nib.load(f_name)
    data = img.get_data()
    datanp = np.asarray(data)
    right_positions = list(zip(*np.where(datanp == 53)))
    left_positions = list(zip(*np.where(datanp == 17)))
    options = [right_positions, left_positions]
    for counter, X_list in enumerate(options):
        X = np.asarray(X_list)
        pca = PCA(n_components=3)
        pca.fit(X)
        eigen_vectors = pca.components_
        # eigen_values = pca.explained_variance_
        if counter == 0:
            eigenvec_right = eigen_vectors[0, :]
        else:
            eigenvec_left = eigen_vectors[0, :]
    return (eigenvec_right, eigenvec_left)


def r2Slicer(f_name):
    """Load image and isolates central slice."""
    import nibabel as nib
    import numpy as np
    img = nib.load(f_name)
    data = img.get_data()
    datanp = np.asarray(data)
    tama = datanp.shape
    z = tama[2]
    zz = np.around(np.divide(z, 2))
    datanpnew = datanp
    datanpnew[:, :, 0:zz-1] = 0
    datanpnew[:, :, zz+1:] = 0
    output_name = 't2_ref_slice.nii.gz'
    new_img = nib.Nifti1Image(datanpnew, img.affine, img.header)
    new_img.set_filename(output_name)
    # nib.save(new_img, full_output_name)
    return (new_img)


def r2ScalarProd(eigenvec_right, eigenvec_left, T2CosDir, subject_id):
    """Extract angle between HC and normal T2 vector."""
    import numpy as np
    import pandas as pd
    vec1 = T2CosDir[0, :]
    vec2 = T2CosDir[1, :] 
    vec3 = np.cross(vec1,vec2) # I am using this value to process the high res moco images.
    T2 = np.cross(vec1, vec3)  # originally: T2 = np.cross(vec1, vec2)
    mod_t2 = np.linalg.norm(T2)
    normaltoT2 = T2/mod_t2
    # estimating modulus
    mod_ev_l = np.linalg.norm(eigenvec_left)
    mod_ev_r = np.linalg.norm(eigenvec_right)
    # estimating dot products
    dotL = np.dot(eigenvec_left, normaltoT2)
    dotR = np.dot(eigenvec_right, normaltoT2)
    # normalising dot products
    normdotL = dotL/mod_ev_l
    normdotR = dotR/mod_ev_r
    # estimating angles
    alpha_left = np.arccos(np.clip(normdotL, -1, 1))
    alpha_right = np.arccos(np.clip(normdotR, -1, 1))
    alpha_left_deg = np.degrees(alpha_left)
    alpha_right_deg = np.degrees(alpha_right)
    # saving values into results table
    filename = '/home/acardenas/hydra-work/project/testQA_small/pandas.csv'
    df = pd.read_csv(filename)
    df = df.set_index('subject')
    df.loc[subject_id, 'alpha_left'] = alpha_left_deg
    df.loc[subject_id, 'alpha_right'] = alpha_right_deg
    # saving values into results table
    df.to_csv(filename, index=True, encoding='utf-8')
    # output_fname = output_dir + '/alpha_left.csv'
    # np.savetxt(output_fname, alpha_left_deg, delimiter=",")
    return (alpha_right_deg, alpha_left_deg)


def r2ApparentAngle(eigenvec_right, eigenvec_left, T2CosDir, T1CosDir,
                    subject_id):
    """Extract angle between HC and normal T2 vector."""
    import numpy as np
    import pandas as pd
    """
    First of all, estimate the projection of the eigenvector onto
    the T1 slice, then we use the same code as before to estimate the
    angle between the apparent eigenvectors and the high res t2 slice.
    """
    v1 = T1CosDir[0, :]
    v2 = T1CosDir[1, :]
    t1 = np.cross(v1, v2)
    mod_t1 = np.linalg.norm(t1)
    n_t1 = t1/mod_t1
    # estimating moduli
    mod_ev_l = np.linalg.norm(eigenvec_left)
    mod_ev_r = np.linalg.norm(eigenvec_right)
    dot_evl = np.dot(eigenvec_left, n_t1)
    dot_evr = np.dot(eigenvec_right, n_t1)
    apparent_l = eigenvec_left - np.multiply(dot_evl, n_t1)
    apparent_r = eigenvec_right - np.multiply(dot_evr, n_t1)
    # From now on, we use these values as the new eigenvectors.
    vec1 = T2CosDir[0, :]
    vec2 = T2CosDir[1, :]
    t2 = np.cross(vec1, vec2)
    mod_t2 = np.linalg.norm(t2)
    normaltoT2 = t2/mod_t2
    # estimating modulus
    mod_ev_l = np.linalg.norm(apparent_l)
    mod_ev_r = np.linalg.norm(apparent_r)
    # estimating dot products
    dotL = np.dot(apparent_l, normaltoT2)
    dotR = np.dot(apparent_r, normaltoT2)
    # normalising dot products
    normdotL = dotL/mod_ev_l
    normdotR = dotR/mod_ev_r
    # estimating angles
    alpha_left = np.arccos(np.clip(normdotL, -1, 1))
    alpha_right = np.arccos(np.clip(normdotR, -1, 1))
    apparent_alpha_left_deg = np.degrees(alpha_left)
    apparent_alpha_right_deg = np.degrees(alpha_right)
    # saving values into results table
    filename = '/home/acardenas/hydra-work/project/testQA_small/pandas.csv'
    df = pd.read_csv(filename)
    df = df.set_index('subject')
    df.loc[subject_id, 'apparent_alpha_left'] = apparent_alpha_left_deg
    df.loc[subject_id, 'apparent_alpha_right'] = apparent_alpha_right_deg
    # saving values into results table
    df.to_csv(filename, index=True, encoding='utf-8')
    # output_fname = output_dir + '/alpha_left.csv'
    # np.savetxt(output_fname, alpha_left_deg, delimiter=",")
    return apparent_alpha_right_deg, apparent_alpha_left_deg


def r2ShapiroandStats(data_vector, tpm, subject_id):
    """Test data normality and generates statistic descriptors."""
    from scipy import stats
    import numpy as np
    import pandas as pd
    [shapiro_W, shapiro_p] = stats.shapiro(data_vector)
    vector_mean = np.nanmean(data_vector)
    vector_median = np.nanmedian(data_vector)
    vector_std = np.nanstd(data_vector)
    filename = '/home/acardenas/hydra-work/project/testQA_small/pandas.csv'
    df = pd.read_csv(filename)
    df = df.set_index('subject')
    if tpm == 3:  # 0 bet
        # CSF
        df.loc[subject_id, 'CSF_mean'] = vector_mean
        df.loc[subject_id, 'CSF_median'] = vector_median
        df.loc[subject_id, 'CSF_std'] = vector_std
        df.loc[subject_id, 'CSF_shapiro_p'] = shapiro_p
    elif tpm == 1:
        # GM
        df.loc[subject_id, 'GM_mean'] = vector_mean
        df.loc[subject_id, 'GM_median'] = vector_median
        df.loc[subject_id, 'GM_std'] = vector_std
        df.loc[subject_id, 'GM_shapiro_p'] = shapiro_p
    elif tpm == 2:
        # WM
        df.loc[subject_id, 'WM_mean'] = vector_mean
        df.loc[subject_id, 'WM_median'] = vector_median
        df.loc[subject_id, 'WM_std'] = vector_std
        df.loc[subject_id, 'WM_shapiro_p'] = shapiro_p
    elif tpm == 6:  # 3 bet
        # Background
        df.loc[subject_id, 'BG_mean'] = vector_mean
        df.loc[subject_id, 'BG_median'] = vector_median
        df.loc[subject_id, 'BG_std'] = vector_std
        df.loc[subject_id, 'BG_shapiro_p'] = shapiro_p
    # updating csv file with new values
    df.to_csv(filename, index=True, encoding='utf-8')
    # return (shapiro_p, vector_mean, vector_median, vector_std, subject_id)


# def r2getclassNimage(spm_class_image):
#    """Select single image out from spm list of list."""
#    comboNimage1tpm, comboNimage2tpm, comboNimage3tpm, comboNimage6tpm = []
#    for idx, session in enumerate(spm_class_image):
#        comboNimage1tpm = (session[1], 1)
#        comboNimage2tpm = (session[2], 2)
#        comboNimage3tpm = (session[3], 3)
#        comboNimage6tpm = (session[6], 6)
#    return comboNimage1tpm, comboNimage2tpm, comboNimage3tpm, comboNimage6tpm


def r2getclassNimage(spm_class_image):
    """Select single image out from spm list of list."""
    comboNimage1tpm = (spm_class_image[0][0], 1)
    comboNimage2tpm = (spm_class_image[1][0], 2)
    comboNimage3tpm = (spm_class_image[2][0], 3)
    comboNimage6tpm = (spm_class_image[5][0], 6)
    return comboNimage1tpm, comboNimage2tpm, comboNimage3tpm, comboNimage6tpm


# def r2getclassNimage(spm_class_image, tpm):
#    """Select single image out from spm list of list."""
#    classNimage = []
#    for idx, session in enumerate(spm_class_image):
#        if idx+1 == tpm:
#            print("++++++ Selected spm image")
#            classNimage = (session[0])
#    return classNimage


def r2HistogramandThreshold(f_name):
    """Estimate most frequent signal value."""
    # at the moment, f_name=['f_name', tpm], I modified it like this to avoid problems for now.
    from nibabel import load
    import numpy as np
    import matplotlib
    matplotlib.use('agg')
    import matplotlib.pyplot as plt
    ff_name=f_name[0]
    # importing necessary libraries
    # loading image into array
    img = load(ff_name).get_data()
    # making 0 values equal to NaN
    img[img == 0] = np.nan
    # reshaping image from 3D matrix to 1D vector
    iimag = np.reshape(img, img.shape[0] * img.shape[1] * img.shape[2])
    fig = plt.figure()
    ax = fig.add_subplot(111)
    # Generating histogram only for those voxels with values different from NaN
    n, bins, patches = ax.hist(iimag[~np.isnan(iimag)],
                               bins='auto', normed=True)
    Max = np.argmax(n)
    HistThre = n[Max]

    return HistThre, ff_name


def r2qi1Count(background_ima, processed_ima, subject_id):
    """Find number of non-zero voxels."""
    import nibabel as nib
    import numpy as np
    import pandas as pd
    orig = nib.load(background_ima)
    oData = orig.get_data()
    orig_vec = np.reshape(oData, oData.shape[0] * oData.shape[1]
                          * oData.shape[2])
    oNum = (orig_vec > 0).sum()
    proc = nib.load(processed_ima)
    pData = proc.get_data()
    proc_vec = np.reshape(pData, pData.shape[0] * pData.shape[1]
                          * pData.shape[2])
    pNum = (proc_vec > 0).sum()
    qi1 = pNum/oNum
    # saving values into results table
    filename = '/home/acardenas/hydra-work/project/testQA_small/pandas.csv'
    df = pd.read_csv(filename)
    df = df.set_index('subject')
    df.loc[subject_id, 'qi1'] = qi1
    return qi1


def r2undoCombo(ComboImagetpm):
    """Split image and tpm info."""
    ima_file = ComboImagetpm[0]
    tpm = ComboImagetpm[1]
    return ima_file, tpm


"""
def r2getT2normal_vector(subject_id):
    Calculate normal vector to T2 slice based on dicom header info
    import os
    import re
    import numpy as np
    folder = '/home/acardenas/hydra_work/project'
    suffix = subject_id + '/t2.dcmhdr'
    fname = os.path.join(folder, 'suffix')
    f = open(fname)
    for line in f:
        if re.match(("0020 0037"), line):
            a = line
    aa = a.split()
    aaa = aa[6]
    Two_vectors = aaa.split('\\')
    Two_V = list(map(float, Two_vectors))
    vec1 = np.asarray(Two_V[0:3])
    vec2 = np.asarray(Two_V[3:])
    normalV = np.cross(vec1, vec2)
    return normalV
"""
